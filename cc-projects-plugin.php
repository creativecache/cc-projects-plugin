<?php
/**
 * Plugin Name: Creative Cache Projects
 * Plugin URI: https://creativecache.co
 * Description: This plugin enables the Creative Cache Projects feature
 * Version: 1.0.0
 * Author: Creative Cache
 * Author URI: https://creativecache.co
 */

defined( 'ABSPATH' ) or die( 'No script please!' );

add_action( 'init', 'cc_projects_init' );
function cc_projects_init() {
	// Add Projects post type
	register_post_type( 'cc_projects',
		array(
			'labels' => array(
				'name' => __( 'Projects' ),
				'singular_name' => __( 'Project' ),
				'add_new'            => __( 'Add New Project' ),
				'add_new_item'       => __( 'Add New Project' ),
				'edit_item'          => __( 'Edit Project' ),
				'new_item'           => __( 'Add New Project' ),
				'view_item'          => __( 'View Project' ),
				'search_items'       => __( 'Search Project' ),
				'not_found'          => __( 'No projects found' ),
				'not_found_in_trash' => __( 'No projects found in trash' )
			),
			'public' => true,
		    'publicly_queryable' => true,
			'has_archive' => true,
			'hierarchal' => false,
			'rewrite' => array('slug' => 'projects','with_front' => false,),
		    'menu_icon' => 'dashicons-portfolio',
		    'show_in_rest' => true,
			'supports' => array( 'title', 'editor', 'excerpt', 'thumbnail', 'revisions' ),
		)
	);

	// Add Projects categories
	register_taxonomy(
		'projects_categories',
		'cc_projects',
		array(
			'label' => __( 'Categories' ),
			'rewrite' => array( 'slug' => 'projects/categories', 'with_front' => false, ),
			'hierarchical' => true,
			'show_in_rest' => true,
		)
	);
}

function my_rewrite_flush() {
	cc_projects_init();
	flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );